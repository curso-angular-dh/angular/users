import {NgModule} from '@angular/core';
import {SecureMainComponent} from './components/secure-main/secure-main.component';
import {SecureLoginComponent} from './components/secure-login/secure-login.component';
import {SecureRoutingModule} from './routing/secure-routing.module';
import {SharedModule} from '../shared/shared.module';
import {LoginHttpService} from './services/login-http.service';

@NgModule({
  declarations: [SecureMainComponent,
    SecureLoginComponent],
  imports: [
    SharedModule,
    SecureRoutingModule
  ],
  providers: [
    LoginHttpService
  ]
})
export class SecureModule {
}
