import {Injectable, Injector} from '@angular/core';
import {Observable} from 'rxjs';
import {LoginRequest} from './body-request/login.request';
import {HttpService} from '../../../bootstrap/http.service';

@Injectable()
export class LoginHttpService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/login';
  }

  public doPost(request: LoginRequest): Observable<any> {
    return this.httpClient().post(this.getUrl(), request);
  }

  protected injector(): Injector {
    return this._injector;
  }

}
