import {Component, OnDestroy, OnInit} from '@angular/core';
import {LobbyUser} from './lobby-user';

@Component({
  selector: 'app-lobby-users',
  templateUrl: './lobby-users.component.html',
  styleUrls: ['./lobby-users.component.scss']
})
export class LobbyUsersComponent implements OnInit, OnDestroy {

  public users: LobbyUser[];

  constructor() {
    this.users = [
      {
        id: '1',
        first_name: 'Alan',
        last_name: 'Quinones',
        email: 'galanq@gmail.com',
        avatar: 'assets/images/logo.png'
      },
      {
        id: '2',
        first_name: 'Jhon',
        last_name: 'Doe',
        email: 'jhon@gmail.com',
        avatar: 'assets/images/logo.png'
      },
      {
        id: '3',
        first_name: 'Mark',
        last_name: 'Stark',
        email: 'mark@gmail.com',
        avatar: 'assets/images/logo.png'
      }
    ];
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

}
