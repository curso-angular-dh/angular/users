import {Routes} from '@angular/router';
import {LobbyMainComponent} from '../components/lobby-main/lobby-main.component';
import {LobbyUsersComponent} from '../components/lobby-users/lobby-users.component';
import {LobbyResourcesComponent} from '../components/lobby-resources/lobby-resources.component';

export const LOBBY_ROUTES: Routes = [
  {
    path: '',
    component: LobbyMainComponent,
    children: [
      {
        path: 'users',
        component: LobbyUsersComponent
      },
      {
        path: 'resources',
        component: LobbyResourcesComponent
      },
      {
        path: '',
        redirectTo: '/lobby/users',
        pathMatch: 'full'
      }
    ]
  }
];
