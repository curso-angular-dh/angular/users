import {HttpConfiguration} from './http.configuration';
import {Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';

export abstract class HttpService extends HttpConfiguration {

  protected abstract injector(): Injector;

  public abstract path(): string;

  public httpClient(): HttpClient {
    return this.injector().get(HttpClient);
  }

  public getUrl(): string {
    return this.getApiAddres() + this.path();
  }

}
